import 'dart:convert';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:katalogbuku_flutter/BukuDetail.dart';
import 'package:katalogbuku_flutter/BukuModel.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

class BukuList extends StatefulWidget {

  final List<Buku> bukudata;
  BukuList(this.bukudata, {Key key});
  
  
  @override
  _BukuListState createState() => _BukuListState();
}

class _BukuListState extends State<BukuList> {
  
  @override
  Widget build(BuildContext context) {
    
    //print("Panjang list buku : " + widget.bukudata.length.toString());
    return GridView.builder(
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: MediaQuery.of(context).orientation == Orientation.portrait?2:3,
        childAspectRatio: 0.7
      ),
      itemCount: widget.bukudata.length == null ? 0: widget.bukudata.length,
      itemBuilder: (BuildContext context, int i){
        Uint8List bytes;
        bytes = base64.decode(widget.bukudata[i].imageBuku);
        return GestureDetector(
          onTap: (){
            Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) => new BukuDetail(widget.bukudata[i], false)));
          },
          child: Card(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Expanded(
                flex: 10,
                child: new Container(
                  decoration: new BoxDecoration(
                  image: new DecorationImage(
                    image: new MemoryImage(bytes),
                    fit: BoxFit.fitWidth,
                  ),
                ),
                )
              ),
              Container(
                padding: EdgeInsets.all(4),
                width: double.infinity,
                child: Text(widget.bukudata[i].judulBuku, style:  TextStyle(fontSize: 14, fontWeight: FontWeight.bold),),
              ),
              Expanded(
                child: SingleChildScrollView(
                child: Container(
                  padding: EdgeInsets.only(left: 4, right: 4, bottom: 2, top: 2),
                  child: Text(widget.bukudata[i].pengarang, style: TextStyle(fontSize: 12),),
                ),
              ),
              ),
            ],
          ),
          
        )
        );
      },
    );
  }
}
class BukuModel {
  final String result;
  final List<Buku> bukus;

  BukuModel({this.result, this.bukus});

  factory BukuModel.fromJson(Map<String, dynamic> parsedJson){

    var list = parsedJson['Buku'] as List;
    print(list.runtimeType);
    List<Buku> bukuList = list.map((i) => Buku.fromJson(i)).toList();


    return BukuModel(
      result: parsedJson['Result'],
      bukus: bukuList
    );
  }
}

class Buku {
  final String idBuku;
  final String isbn;
  final String judulBuku;
  final String pengarang;
  final String tahunBuku;
  final String idkat;
  final String imageBuku;
  final String sinopsisBuku;

  Buku({this.idBuku, this.isbn, this.judulBuku, this.pengarang, this.tahunBuku, this.idkat, this.imageBuku, this.sinopsisBuku});

  factory Buku.fromJson(Map<String, dynamic> parsedJson){
   return Buku(
     idBuku:parsedJson['id_buku'],
     isbn:parsedJson['isbn'],
     judulBuku: parsedJson['judul_buku'],
     pengarang: parsedJson['pengarang'],
     tahunBuku: parsedJson['tahun_buku'],
     idkat: parsedJson['kat_id'],
     imageBuku: parsedJson['image_buku'],
     sinopsisBuku: parsedJson['sinopsis_buku'],
   );
  }
}
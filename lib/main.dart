import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert';
import 'package:katalogbuku_flutter/BukuList.dart';
import 'package:katalogbuku_flutter/BukuDetail.dart';
import 'package:katalogbuku_flutter/BukuModel.dart';
import 'package:http/http.dart' as http;
import 'package:katalogbuku_flutter/Repository/Repository.dart' as br;
import 'package:katalogbuku_flutter/DBHelper.dart';

void main() {
  runApp(new MaterialApp(
    title: "Katalog Buku",
    debugShowCheckedModeBanner: false,
    home: new Home(),
  ));
}

class Home extends StatefulWidget {
  static _HomeState of(BuildContext context) =>
      context.ancestorStateOfType(const TypeMatcher<_HomeState>());

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  //final homestatekey = new GlobalKey<_HomeState>();
  StreamController _getController;
  String bukuJson;
  BukuModel buku;
  final db = new DBHelper();

  Future<void> doPullRefresh() async {
    br.Repository.get().getBuku().then((res) async {

      _getController.add(res);


      return res;
    });
    // setState(() {
    //   print("masok pull refresh");
    //   br.Repository.get().getBuku();
    // });
  }

  _navigateToDetail(BuildContext context) async {
    final result = await Navigator.of(context).push(MaterialPageRoute(
        builder: (BuildContext context) => new BukuDetail(null, true)));

    setState(() {
      print("masok set state");
      loadData();
      this.db.getBuku();
    });
  }

  loadData() async {
    await br.Repository.get().getBuku().then((res) async {
      _getController.add(res);
      return res;
    });

  }
  loadDataLokal() async {
    await db.getBuku().then((res) async {
      _getController.add(res);
      return res;
    });

  }

  @override
  void initState() {
    _getController = new StreamController();
    loadDataLokal();
    loadData();

    // setState(() {
    //   _getController = new StreamController();
    //   loadData();
    //   print("print init state");
    //   br.Repository.get().getBuku();
    // });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        floatingActionButton: FloatingActionButton(
            child: Icon(
              Icons.edit,
              color: Colors.white,
            ),
            backgroundColor: Colors.red[800],
            onPressed: () => _navigateToDetail(context) ),
        appBar: new AppBar(
          leading: Icon(Icons.book),
          title: new Text(
            "Katalog Buku",
            style: TextStyle(
                color: Colors.white, fontSize: 25, fontWeight: FontWeight.w300),
          ),
          backgroundColor: Colors.red[800],
        ),
        backgroundColor: Colors.grey[200],
        body: RefreshIndicator(
          onRefresh: doPullRefresh,
          child: Column(
            children: <Widget>[
              new Container(
                color: Colors.white,
                margin: EdgeInsets.all(4),
                padding: EdgeInsets.all(4),
                child: new TextField(
                  cursorColor: Colors.red,
                  decoration: new InputDecoration(
                    hintText: 'search a book',
                    hintStyle: TextStyle(
                      color: Colors.red,
                    ),
                  ),
                ),
              ),
              new Expanded(
                  child: StreamBuilder(
                stream: _getController.stream,
                builder: (BuildContext context, AsyncSnapshot snapshot) {
                  if (snapshot.hasError) {
                    return Text("Error");
                  }

                  var data = snapshot.data;

                  return snapshot.hasData
                      ? new BukuList(data)
                      : Center(
                          child: CircularProgressIndicator(),
                        );
                },
              )
                  // child: FutureBuilder(
                  //   future: br.Repository.get().getBuku(),
                  //   builder: (context, snapshot) {
                  //     if (snapshot.hasError) {
                  //       return Text("Error");
                  //     }

                  //     var data = snapshot.data;

                  //     return snapshot.hasData
                  //         ? new BukuList(data)
                  //         : Center(
                  //             child: CircularProgressIndicator(),
                  //           );
                  //   },
                  // ),
                  )
            ],
          ),
        ));
  }
}

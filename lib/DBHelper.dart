import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'dart:io' as io;
import 'dart:async';
import 'package:katalogbuku_flutter/BukuModel.dart';

class DBHelper{
  static final DBHelper _instance = new DBHelper.internal();
  DBHelper.internal();

  factory DBHelper() => _instance;
  static Database _db;

  Future<Database> get db async{
    if(_db != null) return _db;
    _db = await setDB();
    return _db;
  }

  setDB() async{
    io.Directory directory = await getApplicationDocumentsDirectory();
    String path = join(directory.path, "bukuDB");
    var dB = await openDatabase(path, version:1, onCreate: _onCreate);
    return dB;
  }

  void _onCreate(Database db, int version) async{
    await db.execute("CREATE TABLE buku_table(id INTEGER PRIMARY KEY, isbn TEXT, judul_buku TEXT, pengarang TEXT, tahun_buku TEXT, image_buku BLOB, sinopsis TEXT)");
    print("DB Created");
  }

  Future<int> saveBuku(Buku myData) async{
    //print("print saveBuku");
    print("masok savebukuDB " + myData.idBuku + " - " + myData.judulBuku);
    var dbClient = await db;
    //print("REPLACE INTO buku_table(id, isbn, judul_buku, pengarang, tahun_buku, image_buku, sinopsis) VALUES(${myData.idBuku},${myData.isbn},${myData.judulBuku},${myData.tahunBuku},${myData.imageBuku},${myData.sinopsisBuku}");
    int res =  await dbClient.rawInsert(
        'INSERT OR REPLACE INTO buku_table(id, isbn, judul_buku, pengarang, tahun_buku, image_buku, sinopsis)'
            ' VALUES(?, ?, ?, ?, ?, ?, ?)',
        [myData.idBuku,myData.isbn,myData.judulBuku,myData.pengarang,myData.tahunBuku,myData.imageBuku,myData.sinopsisBuku]);

    //print("Data Inserted Buku" + myData.idBuku);
    return res;
  }

  Future<List<Buku>> getBuku() async{
    print("masok getbuku DB");
    var dbClient = await db;
    List<Map> list = await dbClient.rawQuery("SELECT * FROM buku_table");
    print("Length in DB : "+list.length.toString());
    List<Buku> listBuku = new List();
    for(int i = 0; i < list.length; i++){
      var dataBuku = new Buku(idBuku: list[i]['id'].toString(), isbn: list[i]['isbn'], judulBuku: list[i]['judul_buku'], pengarang: list[i]['pengarang'], tahunBuku: list[i]['tahun_buku'], imageBuku: list[i]['image_buku'], sinopsisBuku: list[i]['sinopsis'] );
      //var cobadata = new Buku( list[i]['id'].toString(), list[i]['judul_buku'], list[i]['pengarang'], list[i]['sinopsis']);
      listBuku.add(dataBuku);
      //print("Id Buku in Get : " + dataBuku.idBuku + dataBuku.judulBuku);
    }

    //print("print getBuku From DB");
    return listBuku;
  }

  Future<int> deleteBuku(String idbuku) async{
    var dbClient = await db;
    int res = await dbClient.rawDelete("DELETE FROM buku_table WHERE id=?", [idbuku]);
    return res;
  }

}
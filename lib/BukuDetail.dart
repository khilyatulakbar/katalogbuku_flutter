import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:katalogbuku_flutter/BukuModel.dart';
import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
import 'package:image/image.dart' as Img;
import 'package:path_provider/path_provider.dart';
import 'dart:math' as Math;
import 'package:katalogbuku_flutter/Repository/Repository.dart' as br; 
import 'package:katalogbuku_flutter/DBHelper.dart';
import 'package:katalogbuku_flutter/main.dart' as m;


class BukuDetail extends StatefulWidget {
  BukuDetail(this._mybook, this._isNew);
  final Buku _mybook;
  final bool _isNew;
  
  

  @override
  _BukuDetailState createState() => _BukuDetailState();
}

class _BukuDetailState extends State<BukuDetail> {
  String title;
  bool btnSave = false;
  bool btnEdit = true;
  bool btnDelete = true;
  Buku mybook;
  String createDate;
  File _image;
  String base64Image; 
  Uint8List bytes;
  final db = new DBHelper();

  final cIsbn = TextEditingController();
  final cJudul = TextEditingController();
  final cPengarang = TextEditingController();
  final cTahunBuku = TextEditingController();
  final cSinopsis = TextEditingController();

  var now = DateTime.now();
  bool _enabledTextField = true;


  void _saveData(){
    if(widget._isNew){
      Buku bukuku = new Buku(isbn: cIsbn.text, judulBuku: cJudul.text, pengarang: cPengarang.text, tahunBuku: cTahunBuku.text, sinopsisBuku: cSinopsis.text, imageBuku: base64Image );
      //addBuku();
      br.Repository.get().addBuku(bukuku);
      print("before getbuku");
      br.Repository.get().getBuku();
      br.Repository.get().getBuku();
      print("after getbuku");

    }
    else{
      Buku bukuku = new Buku(idBuku: mybook.idBuku, isbn: cIsbn.text, judulBuku: cJudul.text, pengarang: cPengarang.text, tahunBuku: cTahunBuku.text, sinopsisBuku: cSinopsis.text, imageBuku: base64Image );
      br.Repository.get().updateBuku(bukuku);
      //updateBuku();
    }
    print("before navigator close");
    Navigator.of(context).pop();

  }

  void _editData(){
    setState(() {

      title="Edit Note";
      btnEdit = false;
      btnDelete = true;
      btnSave = true;
      _enabledTextField = true;

        });
  }

  void _deleteData(Buku mybook){
    //deleteBuku();
    br.Repository.get().deleteBuku(mybook.idBuku);
    //br.getBuku();
  }

  void _confirmDelete(){
    AlertDialog alertDialog = AlertDialog(
      content: Text("Do you really want to delete this notes?", style: TextStyle(fontSize: 20),),
      actions: <Widget>[
        RaisedButton(
          color: Colors.red,
          child: Text("Delete", style: TextStyle(color: Colors.white),),
          onPressed: (){
            Navigator.pop(context);
            _deleteData(mybook);
            Navigator.pop(context);
          },
        ),
        RaisedButton(
          color: Colors.green,
          child: Text("Cancel", style: TextStyle(color: Colors.white),),
          onPressed: (){
            Navigator.pop(context);
          },
        ),
      ],
    );

    showDialog(context: context, child: alertDialog);
  }

  Future getImage() async {
    var image = await ImagePicker.pickImage(source: ImageSource.camera);
    
    int rand = new Math.Random().nextInt(100000);
    final tempDir = await getTemporaryDirectory();
    final path = tempDir.path;

    Img.Image imageR = Img.decodeImage(image.readAsBytesSync());
    Img.Image smallImg = Img.copyResize(imageR, 200);

    var compressImg = new File("$path/image_$rand.jpg")..writeAsBytesSync(Img.encodeJpg(smallImg, quality: 85));
    
    setState(() {
      _image = image;
      //print('prin in getImage image.path :' + image.path);
      List<int> imageBytes = compressImg.readAsBytesSync();
      base64Image = base64.encode(imageBytes);
      bytes = base64.decode(base64Image);
      print('prin in getImage Base64 :' + base64Image);
    });
  }

  @override
    void initState() {
      br.Repository.get().getBuku();
      if(widget._mybook != null){
        mybook = widget._mybook;
        cIsbn.text = mybook.isbn;
        cJudul.text = mybook.judulBuku;
        cPengarang.text = mybook.pengarang;
        cTahunBuku.text = mybook.tahunBuku;
        cSinopsis.text = mybook.sinopsisBuku;
        title=mybook.judulBuku;
        _enabledTextField = false;
        base64Image = mybook.imageBuku;
        bytes = base64.decode(mybook.imageBuku);
      }
      super.initState();
    }

  @override
  Widget build(BuildContext context) {
    if (widget._isNew) {
      title = "New Book";
      btnSave = true;
      btnEdit = false;
      btnDelete = false;
    }
    return Scaffold(
      appBar: new AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        title: new Center(
          child: new Text(
           title,
           style: new TextStyle(color: Colors.black, fontSize: 20),
         ),
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.close,
              size: 25,
              color: Colors.black,
            ),
            onPressed: ()=>Navigator.pop(context),
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
        children: <Widget>[
          new Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              CreateButton(icon: Icons.save, enable: btnSave, onpress: _saveData,),
              CreateButton(icon: Icons.edit, enable: btnEdit, onpress: _editData,),
              CreateButton(icon: Icons.delete, enable: btnDelete, onpress: _confirmDelete,),
            ],
          ),
          new Padding(
            padding: EdgeInsets.all(10),
            child: new TextFormField(
              enabled: _enabledTextField,
            controller: cIsbn,
            decoration: InputDecoration(
              hintText: "ISBN",
            ),
            style: TextStyle(fontSize: 24, color: Colors.grey[800]),
            maxLines: null,
            keyboardType: TextInputType.text,
          )
          ),
           new Padding(
            padding: EdgeInsets.all(10),
            child: new TextFormField(
              enabled: _enabledTextField,
            controller: cJudul,
            decoration: InputDecoration(
              hintText: "Judul Buku",
            ),
            style: TextStyle(fontSize: 24, color: Colors.grey[800]),
            maxLines: null,
            keyboardType: TextInputType.text,
          )
          ),
           new Padding(
            padding: EdgeInsets.all(10),
            child: new TextFormField(
              enabled: _enabledTextField,
            controller: cPengarang,
            decoration: InputDecoration(
              hintText: "Pengarang",
            ),
            style: TextStyle(fontSize: 24, color: Colors.grey[800]),
            maxLines: null,
            keyboardType: TextInputType.text,
          )
          ),
           new Padding(
            padding: EdgeInsets.all(10),
            child: new TextFormField(
              enabled: _enabledTextField,
            controller: cTahunBuku,
            decoration: InputDecoration(
              hintText: "Tahun Buku",
            ),
            style: TextStyle(fontSize: 24, color: Colors.grey[800]),
            maxLines: null,
            keyboardType: TextInputType.number,
          )
          ),
          new Padding(
            padding: EdgeInsets.all(10),
            child: new TextFormField(
              enabled: _enabledTextField,
            controller: cSinopsis,
            decoration: InputDecoration(
              hintText: "Sinopsis",
            ),
            style: TextStyle(fontSize: 24, color: Colors.grey[800]),
            maxLines: null,
            keyboardType: TextInputType.text,
            textInputAction: TextInputAction.newline,
          )
          ),
          new RaisedButton(
            onPressed: getImage,
            child: new Icon(Icons.add_a_photo),
          ),
          new Center(
            child: bytes == null
                ? new Text('No image selected.')
                :new Image.memory(bytes),
                //: new Image.file(_image),
          ),
        ],
      ),
      ), 
      
      backgroundColor: Colors.white,
    );
  }
}

class CreateButton extends StatelessWidget {
  final IconData icon;
  final bool enable;
  final onpress;

  CreateButton({this.icon, this.enable, this.onpress});
  
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(shape: BoxShape.circle, color: enable? Colors.red[800] : Colors.grey),
      child: IconButton(
        icon: Icon(icon),
        color: Colors.white,
        iconSize: 18,
        onPressed: (){
          if(enable){
            onpress();
          }
        },
      )
    );
  }
}

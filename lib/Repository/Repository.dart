import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert';
import 'package:katalogbuku_flutter/BukuModel.dart';
import 'package:http/http.dart' as http;
import 'package:katalogbuku_flutter/DBHelper.dart';

class Repository {
  static final Repository _repo = Repository._internal() ;

  DBHelper db;

  Repository._internal(){
    db = DBHelper();
  }

  static Repository get() {
    return _repo;
  }

  Future<List<Buku>> getBuku() async {
    BukuModel buku;
    print("masok getbukuREPO");
    http.Response hasil = await http.get(
        Uri.encodeFull("http://192.168.31.124/katalog/api/buku/list_buku"),
        headers: {"Accept": "application/json"});

    final bukuJson = json.decode(hasil.body);
    buku = new BukuModel.fromJson(bukuJson);

      for(int i = 0; i < buku.bukus.length; i++){
        var myData = Buku(idBuku: buku.bukus[i].idBuku, isbn: buku.bukus[i].isbn, judulBuku: buku.bukus[i].judulBuku, pengarang: buku.bukus[i].pengarang, tahunBuku: buku.bukus[i].tahunBuku, imageBuku: buku.bukus[i].imageBuku, sinopsisBuku: buku.bukus[i].sinopsisBuku, idkat: buku.bukus[i].idkat);
        await db.saveBuku(myData);
      }

      //print("print getBuku From API");

    print("Length in JSON : "+buku.bukus.length.toString());
    return await db.getBuku();

  }

  

  Future<BukuModel> deleteBuku(id) async {
    http.Response hasil = await http
        .post("http://192.168.31.124/katalog/api/buku/delete_buku", headers: {
      "Accept": "application/json",
      "Content-Type": "application/x-www-form-urlencoded"
    }, body: {
      'id_buku': id
    });
    await db.deleteBuku(id);
  }

  Future<BukuModel> updateBuku(Buku bukuku) async {
    try{
      print("prepare update");
    http.Response hasil = await http.post("http://192.168.31.124/katalog/api/buku/update_buku",
        headers: {
        "Accept": "application/json",
        "Content-Type": "application/x-www-form-urlencoded"
      },
          body: {
                'id_buku' : bukuku.idBuku,
                'isbn' : bukuku.isbn,
                'judul_buku' : bukuku.judulBuku,
                'pengarang' : bukuku.pengarang,
                'tahun_buku' : bukuku.tahunBuku,
                'sinopsis_buku' : bukuku.sinopsisBuku,
                'kat_id': '1',
                'image_buku': bukuku.imageBuku
          });
          
          getBuku();
          print("UPdated");
    }
    catch(e){
      print(e.toString());
    }
  }

  Future<BukuModel> addBuku(Buku bukuku) async {
    try{
      print("masok addbukuREPO");
    http.Response hasil = await http.post("http://192.168.31.124/katalog/api/buku/tambah_buku",
        headers: {
        "Accept": "application/json",
        "Content-Type": "application/x-www-form-urlencoded"
      },
          body: {
                'isbn' : bukuku.isbn,
                'judul_buku' : bukuku.judulBuku,
                'pengarang' : bukuku.pengarang,
                'tahun_buku' : bukuku.tahunBuku,
                'sinopsis_buku' : bukuku.sinopsisBuku,
                'kat_id': '1',
                'image_buku': bukuku.imageBuku
          });
    }
    catch(e){
      print(e.toString());
    }    
  }
}
